package project.sol.playerlist;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ListCommand implements CommandExecutor {

    private PlayerList plugin;

    public ListCommand(PlayerList plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){

        if(cmd.getName().equalsIgnoreCase("PlayerList")){

            if(!(sender instanceof Player) || !((Player)sender).getName().equals(PlayerList.OWNER)){
                return false;
            }

            if(args.length != 1){
                sender.sendMessage("/playerlist [all/tester/admin/owner]");
                return true;
            }

            if(args[0].equalsIgnoreCase("all")){
                plugin.setMode(0);
                sender.sendMessage("mode is all");
                return true;
            }

            if(args[0].equalsIgnoreCase("tester")){
                plugin.setMode(1);
                sender.sendMessage("mode is tester");
                return true;
            }

            if(args[0].equalsIgnoreCase("admin")){
                plugin.setMode(2);
                sender.sendMessage("mode is admin");
                return true;
            }

            if(args[0].equalsIgnoreCase("owner")){
                plugin.setMode(3);
                sender.sendMessage("mode is owner");
                return true;
            }

        }

        sender.sendMessage("/playerlist [all/tester/admin/owner]");
        return true;
    }

}
