package project.sol.playerlist;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;

public class PlayerListener implements Listener {

    private PlayerList plugin;

    public PlayerListener(PlayerList plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        if (plugin.getMode() == 0) {
            return;
        }

        List<String> list = new ArrayList<>();

        switch (plugin.getMode()) {
            case 1:
                list.addAll(plugin.getWhiteList().getList());
                list.addAll(plugin.getAdminList().getList());
                list.add(PlayerList.OWNER);
                break;
            case 2:
                list.addAll(plugin.getAdminList().getList());
                list.add(PlayerList.OWNER);
                break;
            case 3:
                list.add(PlayerList.OWNER);
                break;
        }

        for (String s : list) {
            if (event.getPlayer().getName().equals(s)) {
                return;
            } else {
                event.getPlayer().kickPlayer("§7§l>§8§l> §b§lSoLRPG §8§l<§7§l<\\n\\n§cサーバーはメンテナンス中です");
            }
        }

    }

}
