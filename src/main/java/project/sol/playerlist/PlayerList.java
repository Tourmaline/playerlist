package project.sol.playerlist;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class PlayerList extends JavaPlugin {

    //0 = all, 1 = tester, 2 = admin, 3 = owner;
    private int mode = 2;
    private WhiteList whiteList;
    private AdminList adminList;

    public static final String OWNER = "Zionsaaan";

    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        getCommand("PlayerList").setExecutor(new ListCommand(this));
        whiteList = new WhiteList();
        adminList = new AdminList();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void setMode(int i){
        mode = i;
    }

    public int getMode(){
        return mode;
    }

    public WhiteList getWhiteList(){
        return whiteList;
    }

    public AdminList getAdminList(){
        return adminList;
    }

}
