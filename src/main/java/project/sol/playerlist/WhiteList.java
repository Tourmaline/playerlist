package project.sol.playerlist;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WhiteList {

    private List<String> list;

    public WhiteList(){
        reload();
    }

    public List<String> getList(){
        return new ArrayList<>(list);
    }

    public void reload(){
        list = new ArrayList<>();
        String str = "";
        try{
            File dir = new File("plugins\\SoL_RPG");
            dir.mkdir();
            File dir2 = new File("plugins\\SoL_RPG\\PlayerList");
            dir2.mkdir();
            File file = new File("plugins\\SoL_RPG\\PlayerList\\player_list.txt");
            file.createNewFile();
            FileReader reader = new FileReader(file);
            BufferedReader bfReader = new BufferedReader(reader);

            str = bfReader.readLine();
            while (str != null){
                list.add(str);
                str = bfReader.readLine();
            }
            bfReader.close();
        } catch (IOException ex){
            System.out.println(ex);
        }
    }

}
